﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ChangeWindowSize
{
    public partial class CWSForm : Form
    {
        const int LEFT_HOTKEY_ID = 1;
        const int RIGHT_HOTKEY_ID = 2;
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
        public CWSForm()
        {
            InitializeComponent();
        }
        private void CWSForm_Load(object sender, EventArgs e)
        {
            try
            {
                RegisterHotKey(this.Handle, LEFT_HOTKEY_ID, 6, (int)Keys.F1);
                RegisterHotKey(this.Handle, RIGHT_HOTKEY_ID, 6, (int)Keys.F2);                
            }
            catch (Exception err)
            {
                label1.Text = err.Message;
            }
            Tray.Icon = SystemIcons.Application;
            Tray.Visible = true;
            
        }
        protected override void WndProc(ref Message m)
        {
            try
            {
                int w = SystemInformation.WorkingArea.Width;
                int h = SystemInformation.WorkingArea.Height;

                if (m.Msg == 0x0312 && m.WParam.ToInt32() == LEFT_HOTKEY_ID)
                {
                    IntPtr handle = GetForegroundWindow();
                    label1.Text = GetActiveWindowTitle();
                    MoveWindow(handle, 0, 0, (int)w / 2, h, true);
                }
                if (m.Msg == 0x0312 && m.WParam.ToInt32() == RIGHT_HOTKEY_ID)
                {
                    IntPtr handle = GetForegroundWindow();
                    label1.Text = GetActiveWindowTitle();
                    MoveWindow(handle, w / 2, 0, (int)w / 2, h, true);
                }
            }
            catch (Exception e)
            {
                label1.Text = e.Message;
            }
            base.WndProc(ref m);
        }
        private string GetActiveWindowTitle()
        {
            const int nChars = 256;
            StringBuilder Buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();

            if (GetWindowText(handle, Buff, nChars) > 0)
            {
                return Buff.ToString();
            }
            return "";
        }
        private void Tray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        private void CWSForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Tray.Visible = true;
                this.Hide();
                Tray.BalloonTipText = "I'm here";
                Tray.ShowBalloonTip(500);
            }
            else
            if (FormWindowState.Normal == this.WindowState)
            {
                Tray.Visible = false;
            }
        }
    }
}


